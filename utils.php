<?php
/** A set of utility functions, for php-generating-html.
 * @author Ian Barland
 * @version 2013.Feb.16
 */

/** Create (the html text for) a link tag.  Use the the URL as the tag-text,.
 * @param $url (string) The URL to link to.
 * @param $linkTxt  (string-or-false) The link-text, or false as a sentinel
 *    indicating that url should also be used as the link-text.
 * @return (the html text for) a link tag.
 */
function hyperlink($url,$linkTxt=false) {
  $theRealLinkTxt = ($linkTxt === false ? $url : $linkTxt);
  return "<a href='$url'>$theRealLinkTxt</a>";
  }

/** Correctly pluralize a noun (or not).
 * @param $num  How many of the $noun we have.
 * @param $noun  The noun to pluralize.
 * @return A string with the number&noun, correctly pluralized.
 */
function pluralize($num, $noun) {
  $theSuffix = (abs($num) == 1 ? "" : "s");
  return "$num $noun$theSuffix";
  }

/** Thumbnail: return HTML for an image with small width, which links to the full-size image.
 * @param $imgURL The URL for the image file.
 * @param $width The desired width for the thumbnail.
 * @return HTML for an image with small width, which links to the full-size image.
 */
function thumbnail( $imgURL, $width ) {
  return hyperlink( $imgURL, "<img src='$imgURL' width='${width}px' />" );
  }

define('SHOW_SUCCESSFUL_TEST_OUTPUT',true);
$testCaseCount = 0;


/** Test that the actual-output-string is as expected.
 * @param $act The actual result from a test-case.
 * @param $exp The expected *prefix* from a test-case.
 * If the test fails, an error message is printed.
 * If the test passes, output is only printed if SHOW_SUCCESSFUL_TEST_OUTPUT.
 * If `$normalize` is set, disregard differences in whitespace and quote-marks (useful for testing strings of HTML).
 */
function test( $act, $exp, $normalize=false ) {
  global $testCaseCount;
  ++$testCaseCount;
  $act2 = $normalize ? normalizeString($act,true) : $act;
  $exp2 = $normalize ? normalizeString($exp,true) : $exp;
  if ($act2  === $exp2) {
    if (SHOW_SUCCESSFUL_TEST_OUTPUT) { echo "." . ($testCaseCount%5 == 0 ? " " : ""); } // Test passed.
    }
  else {
    $divider = (15+strlen($act2)+strlen($exp2) > 120) ? "\n" : " ";
    echo "\n*** test #$testCaseCount FAILED:$divider'$act2'$divider!==$divider'$exp2'.\n";
    }
  }

/** normalizeString: ANY -> ANY
 * If `$val` is a string, then normalize its whitespace:
 * collapse adjacent horiz-whitespace into a single space;
 * trim;
 * convert \r\n into \n;
 * collapse adjacent \n's into just one;
 * If `$foldQuotes` then convert both ' and " to ' -- useful for html testing.
 */
function normalizeString($val, $foldQuotes=false) {
  if (!is_string($val)) {
    return $val;
    }
  else {
    //$val1 = preg_replace('/(\\p{Z}|\\t)+/'," ", $val);
    $val1 = preg_replace("/(\\p{Z}|\\s|\\r|\\n|\\t)+/"," ", $val);
    $val2 = trim($val1);
    $val3 = preg_replace("/\\r\\n/","\n", $val2);
    $val4 = preg_replace("/\\n+/","\n", $val3);
    $val5 = $foldQuotes ? preg_replace('/"/',"'",$val4) : $val4;
    return $val5;
    }
  }




?>
