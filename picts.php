<?php 
error_reporting(E_ALL);
require_once('utils.php'); 
?>

<html>
  <head>
    <title>hw04 pictures</title>
  </head>

  <body>
    <h3>Instructions</h3>
    <ol type='i'>
      <li>Add an image to the hw04/git repo, and link to it in the UL below.
      </li>
      <li>
        You can generate/view the html file via <code>php picts.php >! picts.html</code>.
        (Note that we <em>don't</em> <code>git add</code> the html file to the project,
        since it is generated from the other files in the project.)
      </li>
      <li>
        Make sure you have copyright for any image-files placed in this directory.
        (Just because an image is on the web does <em>not</em> mean you are allowed
        to make a copy of it!)
      </li>
      <li>
        Do NOT add any php code here which attacks <tt>php.radford.edu</tt> via a 
        denial-of-service-attack &mdash; inadvertently or not.
      </li>
    </ol>


    <h3>Images</h3>
    <ul>
      <li>
	Before the ravages of time gave me significant grey:<br/>
        <?php echo thumbnail('ibarland-dept-portrait-2006-Jan.jpg', 100); ?>
      </li>
      <li>
       A cute puppy:<br/>
      <?php echo thumbnail('puppy-1903313_960_720.jpg', 100); ?>
      </li>
			<li>
       A logo I made a long time ago:<br/>
      <?php echo thumbnail('schildrey2Logo.png', 100); ?>
      </li>
      <li>
	Cassidy's lovable dog<br />
	<?php echo thumbnail('cassidydog.jpg',100); ?>
     </li>
	  <li>
	    A neat snail that a friend of mine drew:<br/>
	    <?php echo thumbnail('snail.png', 100); ?>
	  </li>
	  	<li>
		I google image searched 'photo', this is the first result:<br/>
      <?php echo thumbnail('photo.jpeg', 100); ?>
      </li>
      <li>
       A bush in my backyard:<br/>
      <?php echo thumbnail('a_Forsythia.jpg', 100); ?>
      </li>
      <li>
        Haley's husky Steele:<br/>
      <?php echo thumbnail('Steele.jpg', 100); ?>
      </li>
      <li>
        Michael T's favorite cat:<br />
        <?php echo thumbnail('cat.jpg', 100); ?>
      </li>
      <li>
	Say Cheese:<br/>
	<?php echo thumbnail('sayCheese.jpg', 100); ?>
      </li>
      <li>
	She heard the distant sound of a tuna can being opened:<br />
	<?php echo thumbnail('Drew-cat.jpg', 100); ?>
      </li>
      <li>
	A deadly shark:</br>
	<?php echo thumbnail('shark_nene.png', 100); ?>
      </li>
      <li>
        Paige's dog in a cowboy hat too small for his head:<br/>
        <?php echo thumbnail('cowboy-leo.png', 100); ?>
      </li>
	<li>
		Craig's dog Riley<br />
		<?php echo thumbnail('riley.jpg', 100); ?>
	</li>
  <li>
    Batman<br/>
    <?php echo thumbnail('batman.jpg', 100); ?>
    </li>
     <li>
      a man:<br/>
       <?php echo thumbnail('a man.jpg', 100); ?>
    </li>
     <li>
      nice paint:<br/>
       <?php echo thumbnail('paint.jpg',100); ?>
     </li>
    </ul>
  </body>
</html>
